package org.edgegallery.developer.model.lcm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateImage {

    private String name;

    private String metadata;

}
