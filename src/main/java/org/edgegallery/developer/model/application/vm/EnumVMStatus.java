package org.edgegallery.developer.model.application.vm;

public enum EnumVMStatus {
    NOT_DEPLOY,
    DEPLOYED,
    EXPORTED
}
