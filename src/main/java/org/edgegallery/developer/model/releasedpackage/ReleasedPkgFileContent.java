package org.edgegallery.developer.model.releasedpackage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReleasedPkgFileContent {

    private String filePath;

    private String content;
}
