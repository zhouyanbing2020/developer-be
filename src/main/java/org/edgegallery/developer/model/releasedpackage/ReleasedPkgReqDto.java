package org.edgegallery.developer.model.releasedpackage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReleasedPkgReqDto {

    private String appId;

    private String packageId;

}
