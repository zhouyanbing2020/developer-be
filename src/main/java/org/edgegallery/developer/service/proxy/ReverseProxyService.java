/*
 *    Copyright 2020-2021 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.service.proxy;

import org.edgegallery.developer.model.reverseproxy.SshResponseInfo;

public interface ReverseProxyService {

    void addReverseProxy(String hostId, int hostConsolePort, String token);

    void deleteReverseProxy(String hostId, int hostConsolePort, String token);

    String getVmConsoleUrl(String applicationId, String vmId, String userId, String token);

    SshResponseInfo getVmSshResponseInfo(String applicationId, String vmId, String userId, String xsrfValue);

    SshResponseInfo getContainerSshResponseInfo(String applicationId, String userId, String xsrfValue);
}
