<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~    Copyright 2021 Huawei Technologies Co., Ltd.
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software
  ~    distributed under the License is distributed on an "AS IS" BASIS,
  ~    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and
  ~    limitations under the License.
  -->

<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.edgegallery.developer.mapper.application.vm.VMMapper">
  <resultMap id="VMMap" type="org.edgegallery.developer.model.application.vm.VirtualMachine">
    <id property="id" column="id"/>
    <result property="name" column="name"/>
    <result property="flavorId" column="flavor_id"/>
    <result property="imageId" column="image_id"/>
    <result property="targetImageId" column="target_image_id"/>
    <result property="userData" column="user_data"/>
    <result property="status" column="status"/>
    <result property="areaZone" column="area_zone"/>
    <result property="flavorExtraSpecs" column="flavor_extra_specs"/>
  </resultMap>

  <resultMap id="VMPortMap" type="org.edgegallery.developer.model.application.vm.VMPort">
    <id property="id" column="id"/>
    <result property="name" column="name"/>
    <result property="description" column="description"/>
    <result property="networkName" column="network_name"/>
  </resultMap>

  <resultMap id="VMCertificateMap" type="org.edgegallery.developer.model.application.vm.VMCertificate">
    <result property="certificateType" column="certificate_type"/>
    <result property="pwdCertificate" column="pwd_certificate" typeHandler="org.edgegallery.developer.model.handler.JsonTypeHandler"/>
    <result property="keyPairCertificate" column="key_pair_certificate" typeHandler="org.edgegallery.developer.model.handler.JsonTypeHandler"/>
  </resultMap>

  <sql id="VMColumn">
    id, name, flavor_id, image_id, target_image_id, user_data, status, area_zone, flavor_extra_specs
  </sql>

  <sql id="VMPortAllColumn">
    id, name, description, network_name
  </sql>

  <sql id="VMCertificateAllColumn">
    certificate_type, pwd_certificate, key_pair_certificate
  </sql>

  <insert id="createVM">
    insert into tbl_vm (id, app_id, name, flavor_id, image_id, target_image_id, user_data, status, area_zone, flavor_extra_specs)
    values
    ( #{vm.id}, #{applicationId}, #{vm.name}, #{vm.flavorId}, #{vm.imageId}, #{vm.targetImageId}, #{vm.userData}, #{vm.status},  #{vm.areaZone},
    #{vm.flavorExtraSpecs});
  </insert>

  <update id="modifyVM" parameterType="org.edgegallery.developer.model.application.vm.VirtualMachine">
    UPDATE
    tbl_vm
    SET
    name = #{name}, flavor_id = #{flavorId}, image_id = #{imageId}, target_image_id = #{targetImageId},
    user_data = #{userData},
    status = #{status}, area_zone = #{areaZone}, flavor_extra_specs = #{flavorExtraSpecs}
    WHERE
    id = #{id}
  </update>

  <update id="updateVmStatus" >
    UPDATE
    tbl_vm
    SET
    target_image_id = #{targetImageId},
    status = #{status}
    WHERE
    id = #{vmId}
  </update>

  <delete id="deleteVM" parameterType="String">
    DELETE FROM tbl_vm WHERE id = #{id};
    DELETE FROM tbl_vm_instantiate_info WHERE vm_id = #{id};
    DELETE FROM tbl_vm_port_instantiate_info WHERE vm_id = #{id};
    DELETE FROM tbl_vm_image_export_info WHERE vm_id = #{id};
    DELETE FROM tbl_vm_port WHERE vm_id = #{id};
    DELETE FROM tbl_vm_certificate WHERE vm_id = #{id};
  </delete>

  <delete id="deleteAllVMsByAppId" parameterType="String">
    DELETE FROM tbl_vm WHERE where app_id = #{applicationId};
    DELETE FROM tbl_vm_port WHERE app_id = #{applicationId};
    DELETE FROM tbl_vm_certificate WHERE app_id = #{applicationId};
  </delete>

  <select id="getAllVMsByAppId" parameterType="String" resultMap="VMMap">
    SELECT
    <include refid="VMColumn"/>
    FROM tbl_vm where app_id = #{applicationId}
  </select>

  <select id="getVMById" parameterType="String" resultMap="VMMap">
    SELECT
    <include refid="VMColumn"/>
    from tbl_vm where id=#{vmId} and app_id=#{applicationId}
  </select>

  <insert id="createVMPort">
    insert into tbl_vm_port (id, vm_id, name, description, network_name)
    values
    ( #{port.id}, #{vmId}, #{port.name}, #{port.description}, #{port.networkName})
  </insert>

  <update id="modifyVMPort" parameterType="org.edgegallery.developer.model.application.vm.VMPort">
    UPDATE
    tbl_vm_port
    SET
    name = #{name}, description = #{description}
    WHERE
    id = #{id}
  </update>

  <delete id="deleteVMPort" parameterType="String">
    DELETE FROM tbl_vm_port WHERE id = #{id};
  </delete>

  <delete id="deleteAllVMPortsByVMId" parameterType="String">
    DELETE FROM tbl_vm_port WHERE vm_id = #{vmId}
  </delete>

  <select id="getAllVMPorts" resultMap="VMPortMap">
    SELECT
    <include refid="VMPortAllColumn"/>
    FROM tbl_vm_port
  </select>

  <select id="getAllVMPortsByVMId" parameterType="String" resultMap="VMPortMap">
    SELECT
    <include refid="VMPortAllColumn"/>
    FROM tbl_vm_port where vm_id = #{vmId}
  </select>

  <select id="getVMPortById" parameterType="String" resultMap="VMPortMap">
    SELECT
    <include refid="VMPortAllColumn"/>
    FROM tbl_vm_port where id = #{id}
  </select>

  <insert id="createVMCertificate">
    insert into tbl_vm_certificate (vm_id, certificate_type, pwd_certificate, key_pair_certificate)
    values
    (#{vmId}, #{certificate.certificateType},
    #{certificate.pwdCertificate, jdbcType=OTHER, typeHandler=org.edgegallery.developer.model.handler.JsonTypeHandler},
    #{certificate.keyPairCertificate, jdbcType=OTHER, typeHandler=org.edgegallery.developer.model.handler.JsonTypeHandler})
  </insert>

  <update id="modifyVMCertificate">
    UPDATE
    tbl_vm_certificate
    SET
    certificate_type = #{certificate.certificateType},
    pwd_certificate = #{certificate.pwdCertificate, jdbcType=OTHER, typeHandler=org.edgegallery.developer.model.handler.JsonTypeHandler},
    key_pair_certificate = #{certificate.keyPairCertificate, jdbcType=OTHER, typeHandler=org.edgegallery.developer.model.handler.JsonTypeHandler}
    WHERE
    vm_id = #{vmId}
  </update>

  <delete id="deleteVMCertificate" parameterType="String">
    DELETE FROM tbl_vm_certificate WHERE vm_id = #{vmId};
  </delete>

  <select id="getVMCertificate" parameterType="String" resultMap="VMCertificateMap">
    SELECT
    <include refid="VMCertificateAllColumn"/>
    FROM tbl_vm_certificate where vm_id = #{vmId}
  </select>

</mapper>